const express = require('express');
const path = require('path');
const app = express();
const fs = require('fs');
const port = 3000;
var multiline = require('multiline');
const sqlite3 = require('sqlite3').verbose();

const defaultMode = sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE;
const db = new sqlite3.Database('./db/chat.db', defaultMode, function(err) {
    if (err) {
      console.log('Error opening database:', err, err.stack);
      return;
    }
    console.log('Database was opened successfully');
});

// SQL for make table Users.
var SQL_Users = multiline(function() {/*
  CREATE TABLE IF NOT EXISTS "Users" (
      `ID`    INTEGER PRIMARY KEY AUTOINCREMENT,
      `FullName` TEXT,
      `ID_Auth` TEXT
    );
*/});

// SQL for make table Messages.
var SQL_Messages = multiline(function() {/*
  CREATE TABLE IF NOT EXISTS "Messages" (
      `ID`    INTEGER PRIMARY KEY AUTOINCREMENT,
      `ReceiverID` INTEGER,
      `SenderID` INTEGER,
      `Message` TEXT,
      FOREIGN KEY(ReceiverID) REFERENCES Users(ID)
      FOREIGN KEY(SenderID) REFERENCES Users(ID)
    );
*/});

// SQL for make table Channels.
var SQL_Channels = multiline(function() {/*
  CREATE TABLE IF NOT EXISTS "Channels" (
      `ID` INTEGER PRIMARY KEY AUTOINCREMENT,
      `Title` TEXT
    );
*/});

// SQL for make table ChannelsMessages.
var SQL_ChannelsMessages = multiline(function() {/*
  CREATE TABLE IF NOT EXISTS "ChannelsMessages" (
      `ID`    INTEGER PRIMARY KEY AUTOINCREMENT,
      `Receivers` TEXT,
      `ChannelID` INTEGER,
      `Message` TEXT,
      FOREIGN KEY(ChannelID) REFERENCES Channels(ID)
    );
*/});

var serializeSQL = function(SQL) {
  db.serialize(function() {
    db.run(SQL, [], function(err) {
      if (err) {
        console.log('Error executing statement:', err, err.stack);
        return;
      }
    });
  });
}
serializeSQL(SQL_Users);
serializeSQL(SQL_Messages);
serializeSQL(SQL_Channels);
serializeSQL(SQL_ChannelsMessages);


const addNewUser = function(FullName, ID_Auth, callback, callbackError) {
  getUser(`USER_${ID_Auth}`, (result) => {
    if(!result) {
      db.run(`INSERT INTO Users (FullName, ID_Auth) VALUES(?, ?)`, [FullName, `USER_${ID_Auth}`], function(err) {
        if (err) {
          return console.log(err.message);
        }
        callback(this.lastID);
      });
    } else {
      callbackError();
    }
  });
}
const addNewChannel = function(Title, callback) {
  db.run(`INSERT INTO Channels (Title) VALUES(?)`, [Title], function(err) {
    if (err) {
      return console.log(err.message);
    }
    callback(this.lastID);
  });
}
const getUser = function(ID_Auth, callback, callbackError) {
  db.get('SELECT * from Users WHERE ID_Auth = ?', [ID_Auth], (err, result) => {  
    if (err) {
      callbackError();
      console.log(err)
    } else {
      callback(result);
    }
  })
}
const getUsers = function(callback, callbackError) {
  db.all('SELECT * from Users', [], (err, result) => {  
    if (err) {
      callbackError();
    } else {
      callback(result);
    }
  })
}
const sendMessageToUser = function(ReceiverID, SenderID, Message, callback) {
  db.run(`INSERT INTO Messages (ReceiverID, SenderID, Message) VALUES(?, ?, ?)`, [ReceiverID, SenderID, Message], function(err) {
    if (err) {
      return console.log(err.message);
    }
    callback(this.lastID);
  });
}
const getMessages = function(UserID, callback) {
  db.all(`SELECT * from Messages WHERE ReceiverID = "${UserID}" OR SenderID = "${UserID}"`, [], function(err, result) {
    if (err) {
      console.log(err)
    } else {
      callback(result);
    }
  });
}

addNewUser('USER_1_NAME', 1, (id) => {
  console.log(id, 'id');
}, () => {
  console.log('Already used');
});
addNewUser('USER_2_NAME', 2, (id) => {
  console.log(id, 'id');
}, () => {
  console.log('Already used');
});
addNewUser('USER_3_NAME', 3, (id) => {
  console.log(id, 'id');
}, () => {
  console.log('Already used');
});

getUsers((result) => {
  console.log(result, 'result');
});

sendMessageToUser('USER_1', 'USER_2', 'Test message', () => {
  console.log('Sending message');
});
sendMessageToUser('USER_1', 'USER_3', 'Test message', () => {
  console.log('Sending message');
});
sendMessageToUser('USER_2', 'USER_3', 'Test message', () => {
  console.log('Sending message');
});

getMessages('USER_2', (result) => {
  console.log('messages',result);
});

addNewChannel('Channel_1', (Id) => {
  console.log(`Added channel ${Id}`);
});
addNewChannel('Channel_2', (Id) => {
  console.log(`Added channel ${Id}`);
});
addNewChannel('Channel_3', (Id) => {
  console.log(`Added channel ${Id}`);
});
addNewChannel('Channel_4', (Id) => {
  console.log(`Added channel ${Id}`);
});

 // close the database connection
 db.close();