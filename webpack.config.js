"use strict";
const webpack = require('webpack');
const path = require('path');
const loaders = require('./webpack.loaders');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const DashboardPlugin = require('webpack-dashboard/plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const { CheckerPlugin } = require('awesome-typescript-loader');
const SCSSLoader = {
	test: /\.scss$/,
	use: [
		"style-loader",
		{
			loader: 'typings-for-css-modules-loader',
			query: {
				modules: true,
				importLoaders: 1,
				camelCase: 'dashes',
				namedExport: true,
				localIdentName: '[local]'
			}
		},
		"sass-loader"]
};
const HOST = process.env.HOST || "127.0.0.1";
const PORT = process.env.PORT || "4000";
const hotLoader = {
	test: /\.(j|t)sx?$/,
	exclude: /node_modules/,
	use: {
		loader: "babel-loader",
		options: {
			cacheDirectory: true,
			babelrc: false,
			presets: [
				[
					"@babel/preset-env",
					{ targets: { browsers: "last 2 versions" } } // or whatever your project requires
				],
				"@babel/preset-typescript",
				"@babel/preset-react"
			],
			plugins: [
				// plugin-proposal-decorators is only needed if you're using experimental decorators in TypeScript
				["@babel/plugin-proposal-decorators", { legacy: true }],
				["@babel/plugin-proposal-class-properties", { loose: true }],
				"react-hot-loader/babel"
			]
		}
	}
};

module.exports = {
	mode: 'development',
	entry: {
		vendor: [
			// Required to support async/await
			'@babel/polyfill',
		],
		main: ['./src/index'],
	},
  devtool: process.env.WEBPACK_DEVTOOL || 'source-map',
  output: {
    publicPath: '/',
    path: path.join(__dirname, 'public'),
    filename: "[name].js"
  },
  resolve: {
    extensions: ['.js','.ts', '.tsx', '.jsx', ".css", ".scss"]
  },
  module: {
		rules: [...loaders, SCSSLoader, hotLoader]
  },
  devServer: {
    contentBase: "./src/public",
    // do not print bundle build stats
    noInfo: true,
    // enable HMR
    hot: true,
    // embed the webpack-dev-server runtime into the bundle
    inline: true,
    // serve index.html in place of 404 responses to allow HTML5 history
    historyApiFallback: true,
    port: PORT,
    host: HOST
  },
  plugins: [
		new CheckerPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
		new ForkTsCheckerWebpackPlugin(),
    //new webpack.HotModuleReplacementPlugin(),
    new DashboardPlugin(),
    new HtmlWebpackPlugin({
      template: './src/template.html',
      files: {
        css: ['style.css'],
        js: [ "bundle.js"],
      }
    }),
  ]
};
