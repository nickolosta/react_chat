import * as React from "react";
import "./ChatTextInput.scss";

export class ChatTextInput extends React.Component<any, null> {
	constructor(props: any) {
		super(props);
	}
	public render() {
		return (
			<div className="chat-input">
				<input placeholder="Enter message here..." className="chat-input__field" type="text"/>
			</div>
		);
	}
}
