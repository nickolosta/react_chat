import * as React from "react";
import {ChatTextInput} from "../../";
import {ChatMessagesList} from "../../lists";
import "./RightPanel.scss";

export class RightPanel extends React.Component<any, null> {
	constructor(props: any) {
		super(props);
	}
	public render() {
		return (
			<div className="right_panel">
				<ChatMessagesList></ChatMessagesList>
				<ChatTextInput></ChatTextInput>
			</div>
		);
	}
}
