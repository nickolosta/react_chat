import * as React from "react";
import "./LeftPanel.scss";

export class LeftPanel extends React.Component<any, null> {
	constructor(props: any) {
		super(props);
	}
	public render() {
		return (
			<div className="left-panel">
				<h3 className="left-panel__header"><span className="left-panel__menu-user-label hidden_mobile_version">Classroom 1</span></h3>
				<div className="left-panel__users">
					<h3 className="left-panel__users-header">U<span className="hidden_mobile_version">sers:</span></h3>
					<a className="left-panel__menu-user"># <span className="left-panel__menu-user-label hidden_mobile_version">User 1</span></a>
					<a className="left-panel__menu-user"># <span className="left-panel__menu-user-label hidden_mobile_version">User 2</span></a>
					<a className="left-panel__menu-user"># <span className="left-panel__menu-user-label hidden_mobile_version">User 3</span></a>
					<a className="left-panel__menu-user"># <span className="left-panel__menu-user-label hidden_mobile_version">User 4</span></a>
					<a className="left-panel__menu-user"># <span className="left-panel__menu-user-label hidden_mobile_version">User 5</span></a>
					<a className="left-panel__menu-user"># <span className="left-panel__menu-user-label hidden_mobile_version">User 6</span></a>
				</div>
				<div className="left-panel__users">
					<h3 className="left-panel__channels-header">C<span className="hidden_mobile_version">hannels:</span></h3>
					<a className="left-panel__menu-user"># <span className="left-panel__menu-user-label hidden_mobile_version">Channel 1</span></a>
					<a className="left-panel__menu-user"># <span className="left-panel__menu-user-label hidden_mobile_version">Channel 2</span></a>
					<a className="left-panel__menu-user"># <span className="left-panel__menu-user-label hidden_mobile_version">Channel 3</span></a>
					<a className="left-panel__menu-user"># <span className="left-panel__menu-user-label hidden_mobile_version">Channel 4</span></a>
					<a className="left-panel__menu-user"># <span className="left-panel__menu-user-label hidden_mobile_version">Channel 5</span></a>
					<a className="left-panel__menu-user"># <span className="left-panel__menu-user-label hidden_mobile_version">Channel 6</span></a>
				</div>
			</div>
		);
	}
}
