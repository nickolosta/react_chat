import * as React from "react";
import { hot } from "react-hot-loader";
import {LeftPanel, RightPanel} from "../../";
import "./App.scss";

const App = () => (
	<div className="chat_app">
		<LeftPanel></LeftPanel>
		<RightPanel></RightPanel>
	</div>
);

export default hot(module)(App);
