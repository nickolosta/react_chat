import * as React from "react";
import {ChatMessage} from "../../../presentational";
import "./ChatMessagesList.scss";

export class ChatMessagesList extends React.Component<any, null> {
	constructor(props: any) {
		super(props);
	}
	public render() {
		return (
			<div className="chat-messages">
				<ChatMessage message={{text: "Test 1", dateTime: "", isAnswer: true}}/>
				<ChatMessage message={{text: "Test 2", dateTime: "", isAnswer: true}}/>
				<ChatMessage message={{text: "Test 3", dateTime: "", isAnswer: false}}/>
			</div>
		);
	}
}
