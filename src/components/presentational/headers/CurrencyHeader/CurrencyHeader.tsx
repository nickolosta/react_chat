import * as React from "react";
import { uniqueId } from "../../../../lib/Utilities";
import "./CurrencyHeader.scss";

interface ComponentProps {
	callbackActions: (type: string, data: any ) => any;
}

export class CurrencyHeader extends React.Component<ComponentProps, null> {
	constructor(props: any) {
		super(props);
		this.bindAll();
	}
	private bindAll() {
		this.handleClick = this.handleClick.bind(this);
	}
	private handleClick(event: React.MouseEvent<HTMLElement>) {
		event.stopPropagation();
		this.props.callbackActions("CURRENCY_ITEM.ADD", {id: uniqueId(), ask: 0, bid: 0, symbol: ""});
		this.setFocus();
	}
	private setFocus(){
		setTimeout(() => {
			const elements: any = document.getElementsByClassName("course-currency__item-ask_focus");
			if (elements[0]) {
				elements[0].focus();
			}
		}, 500);
	}
	public render() {
		return (
			<div className="currency__header">
				<div className="currency__header-cell">Symbol</div>
				<div className="currency__header-cell">Bid LP</div>
				<div className="currency__header-cell">Bid</div>
				<div className="currency__header-cell">Ask LP</div>
				<div className="currency__header-cell">Ask</div>
				<div className="currency__header-cell">Spread</div>
				<div onClick={this.handleClick} className="currency__header-cell currency__header-button">+</div>
			</div>
		);
	}
}
