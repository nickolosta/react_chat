import * as classnames from "classnames";
import * as React from "react";
import "./ChatMessage.scss";

interface ComponentProps {
	message: {
		text: string,
		dateTime: string,
		isAnswer: boolean,
	};
}

export class ChatMessage extends React.Component<ComponentProps, null> {
	constructor(props: any) {
		super(props);
	}
	public renderUserIcon() {
		return this.props.message.isAnswer ? <img className="chat-message__user-icon" src="https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?resize=256%2C256&quality=100&ssl=1"/> : null;
	}
	public render() {
		return (
			<div className={
				classnames(
					{
						"chat-message": true,
						"is-answer": this.props.message.isAnswer,
					},
				)}>
				{this.renderUserIcon()}
				<div className="chat-message-text">{this.props.message.text}</div>
			</div>
		);
	}
}
