export interface CurrencyItemInterface {
	ask: number;
	bid: number;
	id: string;
	symbol: string;
}
