import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./components/containers/main/App/App";
import "./styles.scss";

ReactDOM.render(
	<App/>,
	document.getElementById("app"),
);
