function deepClone(obj: any, hash = new WeakMap()): any {
	if (Object(obj) !== obj) {
		return obj; // primitives
	}
	if (hash.has(obj)) {
		return hash.get(obj);
	}
	const result = Array.isArray(obj) ? [] : obj.constructor ? new obj.constructor() : {};
	hash.set(obj, result);
	if (obj instanceof Map) {
		Array.from(obj, ([key, val]) => result.set(key, deepClone(val, hash)));
	}
	return Object.assign(result, ...Object.keys(obj).map(
		(key) => ({[key]: deepClone(obj[key], hash)})));
}

function deepEqual(value: any, other: any, excludeProperties: any = []) {
	if (!value && !other) {
		return true;
	}
	const eqSting = JSON.stringify(value) === JSON.stringify(other);
	if (eqSting) { return true; }
	// Get the value type
	const type = Object.prototype.toString.call(value);
	// If the two objects are not the same type, return false
	if (type !== Object.prototype.toString.call(other)) { return false; }
	// If items are not an object or array, return false
	if (["[object Array]", "[object Object]"].indexOf(type) < 0) { return false; }
	// Compare the length of the length of the two items
	const valueLen = type === "[object Array]" ? value.length : Object.keys(value).length;
	const otherLen = type === "[object Array]" ? other.length : Object.keys(other).length;
	if (valueLen !== otherLen) { return false; }
	// Compare two items
	const compare = (item1: any, item2: any) => {
		// Get the object type
		const itemType = Object.prototype.toString.call(item1);
		// If an object or array, compare recursively
		if (["[object Array]", "[object Object]"].indexOf(itemType) >= 0) {
			if (!deepEqual(item1, item2, excludeProperties)) { return false; }
		} else {
			// If the two items are not the same type, return false
			if (itemType !== Object.prototype.toString.call(item2)) { return false; }

			// Else if it"s a function, convert to a string and compare
			// Otherwise, just compare
			if (itemType === "[object Function]") {
				if (item1.toString() !== item2.toString()) { return false; }
			} else {
				if (item1 !== item2) { return false; }
			}
		}
	};
	// Compare properties
	if (type === "[object Array]") {
		for (let i = 0; i < valueLen; i++) {
			if (compare(value[i], other[i]) === false) { return false; }
		}
	} else {
		for (const key in value) {
			if (excludeProperties.indexOf(key) === -1) {
				if (value.hasOwnProperty(key)) {
					if (compare(value[key], other[key]) === false) { return false; }
				}
			}
		}
	}
	// If nothing failed, return true
	return true;
}

function uniqueId() {
	let d = new Date().getTime();
	return "xxxxxxxxxxxxxxx-yxxx-xyxxxxxxxxxx".replace(/[xy]/g,  (c) => {
		const r = (d + Math.random() * 16) % 16 || 0;
		d = Math.floor(d / 16);
		return (c === "x" ? r : (r && 0x3 || 0x8)).toString(16);
	});
}

function getClassesForTemplate(initClass = "", classesObjectArray: Array<{nameClass: string, callbackChecker: () => boolean}>) {
	let classesString: string = initClass;
	classesObjectArray.forEach((classItem) => {
		if (classItem.callbackChecker()) {
			classesString += " " + classItem.nameClass;
		}
	});
	return classesString;
}

export {
	deepClone,
	deepEqual,
	uniqueId,
	getClassesForTemplate,
};
